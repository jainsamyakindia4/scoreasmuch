import pygame
from pygame.sprite import Sprite
import random

x = 800
y = 600
n = 5
up = ((y-(20*n))/n)  # River Width.
up2 = ((y-(20*n))/n)+20  # River Width + Platform Width.

pygame.font.init()

font = pygame.font.Font('freesansbold.ttf', 32)
textX = 10
textY = 10
black = (0, 0, 0)
blue = (0, 0, 255)
maroon = (128, 0, 0)
text1 = "GAME OVER"
text2 = "DEAD TIME UP"
font = pygame.font.Font('freesansbold.ttf', 32)
over_font = pygame.font.Font('freesansbold.ttf', 64)
