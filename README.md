2019101013

We have 5 partitions in the river.The player keeps moving to right and left according to river flow governed by arrow keys.

The player dies on touching obstacle and score reduces by 5.

Time penalty is calculated and subtracted from the score.

After every round winner is calculated and for next round speed of ships increases for both players.

If any player reaches the end then "Good" is printed on console and game ends there and then.

The score for each player is displayed on top left which depends on its position and time taken.

The final score for each player is also displayed on the console along with time penalty.

After each round screen pauses for 1 seconds so that score can be seen.

You get a 0 if you don't reach the end in 30 seconds.

Instructions:
Player 1 : Move by arrow keys
Player 2 : Move by W-A-S-D
Don't touch any obstacle.All the Best!!!

Sound added in background and at explosion.

BEWARE OF RIVER FLOW!!!

Files
1.main.py - Main Game
2.cloud.py - Fixed Obstacle
3.heart.py - Fixed Obstacle
4.player.py - Player 1 class and functions
5.player2.py - Player 2 class and functions
6.config.py - Configuration contains fonts and images used
7.enemy.py - Class and functions for ships



 