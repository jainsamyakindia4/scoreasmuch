import pygame
from pygame.sprite import Sprite
import random
from config import *

enemyY = []
for i in range(n):
    enemyY.append(((2*i)+1)*((up/2)+10))


class Enemy(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('boat.png')
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(5, 750)
        self.rect.y = random.choice(enemyY)
        self.x_change = 1

    def update(self):
        self.rect.x += self.x_change
        if self.rect.x >= 800:
            self.rect.x = 0
