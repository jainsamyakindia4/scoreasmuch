import random
import math
import sys
import pygame
from pygame import mixer
from player import Player
from enemy import Enemy
from heart import Heart
from cloud import Cloud
from config import *
from time import sleep
from player2 import Player2
count = 1
# Initialize the pygame.

# Overall game loop has 2 main functions for player1 and player2.


def game_loop():
    global count
    pygame.init()
    clock = pygame.time.Clock()
    start_ticks = pygame.time.get_ticks()
    screen = pygame.display.set_mode((x, y))
    screen.fill(blue)

    for i in range(n):
        pygame.draw.rect(screen, maroon, (0, (i*(y/n))-10, x, 20))

    pygame.draw.rect(screen, maroon, (0, y-20, x, 20))
    cloud = pygame.image.load('cloud.png')
    heart = pygame.image.load('heart.png')
    # Sound
    pygame.mixer.music.load("background.wav")
    pygame.mixer.music.play(-1)

    # Caption and Icon
    pygame.display.set_caption("ScoreAsMuch")
    icon = pygame.image.load('shark.png')
    pygame.display.set_icon(icon)

    # declarations
    player = pygame.sprite.Group()
    ships = pygame.sprite.Group()
    all_sprites = pygame.sprite.Group()
    hearts = pygame.sprite.Group()
    clouds = pygame.sprite.Group()
    p = Player(y-10)
    player.add(p)
    all_sprites.add(p)

    player2 = pygame.sprite.Group()
    p2 = Player2(10)
    player2.add(p2)

    for i in range(n+8):
        ship = Enemy()
        ships.add(ship)
        all_sprites.add(ship)

    for ship in ships:
        ship.x_change = count

    count += 1
    # Obstacle-Heart Declarations.
    h1 = Heart(x/5, 4*(up+30))
    hearts.add(h1)
    all_sprites.add(h1)
    h2 = Heart(4*(x/5), 10)
    hearts.add(h2)
    all_sprites.add(h2)
    h3 = Heart(x/6, (2*(up+30)))
    hearts.add(h3)
    all_sprites.add(h3)

    # Obstacle Cloud Declarations.
    c1 = Cloud(x/4, up+30)
    clouds.add(c1)
    all_sprites.add(c1)
    c2 = Cloud((3/4)*x, 2*(up+30))
    clouds.add(c2)
    all_sprites.add(c2)
    c3 = Cloud(x/2, 3*(up+30))
    clouds.add(c3)
    all_sprites.add(c3)

    pygame.font.init()
    flag3 = 0

    def winner():
        if p.score_value > p2.score_value:
            print_winner("WINNER ", 1)
            pygame.time.wait(3000)
        elif p.score_value < p2.score_value:
            print_winner("WINNER ", 2)
            pygame.time.wait(3000)
        elif p.score_value == p2.score_value:
            print_winner("BOTH WIN ", 0)
            pygame.time.wait(3000)

    def print_winner(text, i):
        over_text = over_font.render(str(text) + str(i), True, (255, 255, 255))
        screen.blit(over_text, (200, 250))

    def game_over_text():
        over_text = over_font.render(text1, True, (255, 255, 255))
        screen.blit(over_text, (200, 250))

    def time_up_text():
        up_text = over_font.render(text2, True, (255, 255, 255))
        screen.blit(up_text, (200, 250))

    def show_score1():
        score = font.render("P1 " + str(p.score_value), True, (255, 255, 255))
        screen.blit(score, (textX, textY))

    def show_score2():
        score = font.render("P2 " + str(p2.score_value), True, (255, 255, 255))
        screen.blit(score, (textX, textY))

    def player2_call():
        running2 = True
        start_ticks = pygame.time.get_ticks()
        while running2:
            flag = 0
            screen.fill(blue)
            for i in range(n):
                pygame.draw.rect(screen, maroon, (0, (i*(y/n))-10, x, 20))

            pygame.draw.rect(screen, maroon, (0, y-10, x, 20))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    sys.exit(0)

            p2.update()
            player2.draw(screen)

            for ship in ships:
                ship.update()
            ships.draw(screen)

            hearts.draw(screen)
            clouds.draw(screen)
            # Used Sprite for collision detection.
            player_c = pygame.sprite.groupcollide(player2, ships, False, False)
            heart_c = pygame.sprite.groupcollide(player2, hearts, False, False)
            cloud_c = pygame.sprite.groupcollide(player2, clouds, False, False)
            if player_c or heart_c or cloud_c:
                p2.rect.x = 400
                p2.rect.y = 10
                p2.score_value -= 5
                pygame.mixer.music.load("explosion.wav")
                pygame.mixer.music.play()
                seconds2 = (pygame.time.get_ticks()-start_ticks)/1000
                p2.score_value -= math.floor((seconds2)/2)
                print("Time penalty player 2", math.floor((seconds2)/2))
                show_score2()
                print("Final score 2", p2.score_value)
                pygame.time.wait(1000)
                winner()
                pygame.time.wait(1000)
                flag = 1
                running2 = False  # Break from loop.

            seconds = (pygame.time.get_ticks()-start_ticks)/1000
            if seconds > 30:
                print("Time up")
                p2.score_value = 0
                show_score2()
                print("Final score 2", p2.score_value)
                winner()
                flag = 1
                running2 = False
            show_score2()
            pygame.display.flip()
            if flag == 1:
                p.rect.y = y-10
                game_loop()  # Start again.

    # Game Loop
    def player1_call():
        running = True
        start_ticks = pygame.time.get_ticks()
        while running:
            flag2 = 0
            # RGB = Red, Green, Blue
            screen.fill(blue)
            for i in range(n):
                pygame.draw.rect(screen, maroon, (0, (i*(y/n))-10, x, 20))

            pygame.draw.rect(screen, maroon, (0, y-10, x, 20))
            # Background Image
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    sys.exit(0)

            p.update()
            player.draw(screen)
            # Enemy Movement

            for ship in ships:
                ship.update()
            ships.draw(screen)

            hearts.draw(screen)
            clouds.draw(screen)

            player_co = pygame.sprite.groupcollide(player, ships, False, False)
            heart_co = pygame.sprite.groupcollide(player, hearts, False, False)
            cloud_co = pygame.sprite.groupcollide(player, clouds, False, False)
            if player_co or heart_co or cloud_co:  # Check for collision.
                p.rect.x = 400   # Go to original position.
                p.rect.y = 590
                p.score_value -= 5  # Score reduce.
                pygame.mixer.music.load("explosion.wav")  # Explosion sound.
                pygame.mixer.music.play()
                game_over_text()
                seconds1 = (pygame.time.get_ticks()-start_ticks)/1000
                p.score_value -= math.floor(seconds1/2)
                print("Time penalty player 1", math.floor(seconds1/2))
                print("Final score 1", p.score_value)
                show_score1()
                pygame.time.wait(1000)  # Pause for 1 second.
                flag2 = 1  # Flag used for checking if next player chance.
                running = False  # Break from loop.
                sleep(2)  # Again used for holding screen.

            seconds = (pygame.time.get_ticks()-start_ticks)/1000
            if seconds > 30:  # Check for time up.
                print("Time up")
                time_up_text()
                p.score_value = 0  # Score=0.
                print("Final score 1", p.score_value)
                show_score1()
                flag2 = 1
                sleep(2)
                running = False

            show_score1()
            pygame.display.flip()
            if flag2 == 1:
                p2.rect.y = 10
                player2_call()  # Give chance to 2nd player.

    player1_call()  # Used for 1st Round.

game_loop()  # Initially called.
