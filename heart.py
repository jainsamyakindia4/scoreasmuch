import pygame
from pygame.sprite import Sprite
import random
from config import *


class Heart(pygame.sprite.Sprite):

    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('heart.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
