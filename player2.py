import sys
import math
import pygame
from pygame import mixer
import random
from config import *
pygame.font.init()


class Player2(pygame.sprite.Sprite):

    def __init__(self, height):
        super().__init__()
        self.image = pygame.image.load('player2.png')
        self.rect = self.image.get_rect()
        self.x = x/2
        self.y = height
        self.rect.x = x/2
        self.rect.y = height

        self.X_change = 0
        self.Y_change = 0
        self.score_value = 0

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_a]:
            self.X_change = -1
            self.Y_change = 0
        if keys[pygame.K_d]:
            self.X_change = 1
            self.Y_change = 0
        elif keys[pygame.K_w]:
            self.X_change = 0
            self.Y_change = -1
        elif keys[pygame.K_s]:
            self.X_change = 0
            self.Y_change = 1
        self.rect.x += self.X_change
        if self.rect.x >= x-20:
            self.rect.x = x-20
            self.x = x-20
        # Ensuring Player doesn't go out of bounds.
        elif self.rect.x <= 0:
            self.rect.x = 0
            self.x = 0

        self.rect.y += self.Y_change
        if self.rect.y >= y-10:
            print("Good")
            sys.exit(0)
        elif self.rect.y <= 0:
            self.rect.y = 0
            self.y = 0

        self.score_value = math.floor(10000 / (600 - self.rect.y) + 1)
        # Subtracted from 600(height) to ensure uniformity.
        # +1 to ensure no division by zero.

    def player_change(self):
        self.X_change = 0
        self.Y_change = 0
