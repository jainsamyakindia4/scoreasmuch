import pygame
from pygame.sprite import Sprite
from config import *
import random


class Cloud(pygame.sprite.Sprite):

    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('cloud.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
